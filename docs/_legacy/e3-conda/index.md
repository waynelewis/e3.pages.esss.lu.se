# ESS EPICS Environment (E3)

ESS EPICS Environment (E3) is based on the concept from
[PSI](https://github.com/paulscherrerinstitute/require) of dynamically
loading EPICS modules.

E3 installation is managed using [conda].

```{toctree}
---
maxdepth: 2
caption: Using E3
---
using/quickstart.md
using/module_creation.md
using/recipe_creation.md
```

```{toctree}
---
maxdepth: 2
caption: References
---
references/conda.md
references/requirements.md
```

[conda]: https://docs.conda.io/en/latest/
