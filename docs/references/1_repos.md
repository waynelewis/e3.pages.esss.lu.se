# Repositories

The main repository for e3 is:

<https://gitlab.esss.lu.se/e3/e3>

---

Generally, all ESS e3-related repositories are kept under:

<https://gitlab.esss.lu.se/e3>

They are then furthermore divided into different groups, depending on their type and application. Some examples for e3 wrappers are:

- <https://gitlab.esss.lu.se/e3/common>
- <https://gitlab.esss.lu.se/e3/area>
- <https://gitlab.esss.lu.se/e3/ecat>

The source modules (including applications and libraries) are kept under:

<https://gitlab.esss.lu.se/epics-modules>

And IOCs are kept under:

- <https://gitlab.esss.lu.se/ioc>
- <https://gitlab.esss.lu.se/ioc-lab>
- <https://gitlab.esss.lu.se/ioc-test>
- <https://gitlab.esss.lu.se/ioc-prod>

